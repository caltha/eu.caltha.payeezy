# Payeezy Payment Processor for CiviCRM #

Information about payment processor: https://www.polcard.pl/content/polcard/strona-glowna/payeezy-platnosci-online.html (in Polish)

Information about CiviCRM: http://civicrm.org.pl (in Polish), http://civicrm.org (in English)

# Setup instructions #

Install the extension as usual (manual installation instructions provided here: https://wiki.civicrm.org/confluence/display/CRMDOC/Extensions#Extensions-Installinganewextension).

Development of this extension was supported by Panoptykon Foundation (http://panoptykon.org).
Extension developed by Caltha (http://caltha.eu)

# Procesor płatności Payeezy dla CiviCRM

Informacja o dostawcy płatności online: https://www.polcard.pl/content/polcard/strona-glowna/payeezy-platnosci-online.html

Informacje o CiviCRM: http://civicrm.org.pl, http://civicrm.org (po angielsku).

# Instrukcja instalacji

Standardowa instalacja rozszerzenia (instrukcja manualnej instalacji zgodna z: https://wiki.civicrm.org/confluence/display/CRMDOC/Extensions#Extensions-Installinganewextension).

Stworzenie tego rozszerzenia było możliwe dzięki wsparciu Fundacji Panoptykon (http://panoptykon.org). Rozszerzenie zostało napisane przez Caltha (http://caltha.eu).