<?php

require_once 'CRM/Core/Page.php';

class CRM_Payeezy_Page_Negative extends CRM_Core_Page {
  function run() {
    CRM_Payeezy_Hooks::alterPageRun($this, get_class());
    $orderId = CRM_Utils_Request::retrieve('order_id', 'String', $this, false);
    $responseCode = CRM_Utils_Request::retrieve('response_code', 'String', $this, false);
    $contribution = new CRM_Payeezy_Contribution($orderId);
    $this->assign('orderId', $orderId);
    $this->assign('responseCode', $responseCode);
    $this->assign('total_amount', $contribution->result->total_amount);
    $this->assign('contribution_page_id', $contribution->result->contribution_page_id);
    parent::run();
  }
}
