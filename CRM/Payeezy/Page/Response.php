<?php

require_once 'CRM/Core/Page.php';

class CRM_Payeezy_Page_Response extends CRM_Core_Page {

  /**
   * Call from those IP addresses is allowed.
   *
   * @var array
   */
  private $allowedIP = [
    '193.25.161.161', // todo remove old address after 10.09.2019r.
    '193.25.161.11',
    '193.25.161.75',
  ];

  /**
   * @return null|void
   * @throws \CRM_Core_Exception
   */
  function run() {
    $post = trim(file_get_contents('php://input'));
    if ($post) {
      CRM_Payeezy_Log::add($post);
    }

    if (!$this->isAllowedCall()) {
      $this->handleWarning();
      header('HTTP/1.1 200 Ok');
      echo "INVALID_REMOTE_ADDRESS";
      CRM_Utils_System::civiExit();
    }
    CRM_Utils_System::setTitle(ts('Response'));
    $orderId = CRM_Utils_Request::retrieve('order_id', 'String', $this, false);
    $responseCode = CRM_Utils_Request::retrieve('response_code', 'String', $this, false);
    $contribution = new CRM_Payeezy_Contribution($orderId);
    $controlData = CRM_Utils_Request::retrieve('controlData', 'String', $this, false);
    $sessionId = CRM_Utils_Request::retrieve('session_id', 'String', $this, false);
    $posId = CRM_Utils_Request::retrieve('pos_id', 'String', $this, false);
    $paramsControlData = array(
      'session_id' => $sessionId,
      'invoiceID' => $orderId,
      'amount' => $contribution->result->total_amount,
      'response_code' => $responseCode,
      'transaction_id' => CRM_Utils_Request::retrieve('transaction_id', 'String', $this, false),
      'cc_number_hash' => CRM_Utils_Request::retrieve('cc_number_hash', 'String', $this, false),
      'bin' => CRM_Utils_Request::retrieve('bin', 'String', $this, false),
      'card_type' => CRM_Utils_Request::retrieve('card_type', 'String', $this, false),
      'auth_code' => CRM_Utils_Request::retrieve('auth_code', 'String', $this, false),
    );
    $query = "SELECT password AS shared_key
              FROM civicrm_payment_processor
              WHERE user_name = %1 AND is_test = %2
              LIMIT 1";
    $params = array(
      1 => array($posId, 'String'),
      2 => array($contribution->result->is_test, 'Integer'),
    );
    $sharedKey = CRM_Core_DAO::singleValueQuery($query, $params);
    $processor = array(
      'user_name' => $posId,
      'is_test' => $contribution->result->is_test,
    );
    $request = CRM_Payeezy_ResponseService::prepare($processor, $paramsControlData);
    $controlDataCalculated = CRM_Payeezy_ControlData::calculateServiceResponseControlData($sharedKey, $request);
    if ($controlData == $controlDataCalculated) {
      if ($contribution->result->id) {
        switch ($responseCode) {
          case CRM_Payeezy_ResponseCode::ZAKONCZONA:
          case CRM_Payeezy_ResponseCode::ZATWIERDZONA_DO_ROZLICZENIA:
            $contribution->setCompleted($contribution->result->id);
            break;

          case CRM_Payeezy_ResponseCode::ODRZUCONA:
            $contribution->setFailed($contribution->result->id);
            break;

          default:
            CRM_Core_Error::debug_var('RESPONSE UNSUPPORTED $responseCode', $responseCode);
            CRM_Core_Error::debug_var('RESPONSE $orderId', $orderId);
        }
      }
    } else {
      CRM_Core_Error::debug_var('UNVALID CONTROL DATA $paramsControlData', $paramsControlData);
      CRM_Core_Error::debug_var('UNVALID CONTROL DATA $controlData', $controlData);
      CRM_Core_Error::debug_var('UNVALID CONTROL DATA $controlDataCalculated', $controlDataCalculated);
    }
    header('HTTP/1.1 200 Ok');
    CRM_Utils_System::civiExit();
  }

  /**
   * Check if call comes from allowed IP address.
   *
   * @return bool
   */
  private function isAllowedCall() {
    return (in_array($_SERVER['REMOTE_ADDR'], $this->allowedIP));
  }

  /**
   * Log warning
   */
  private function handleWarning() {
    CRM_Core_Error::debug_log_message('eu.caltha.payeezy warning: Remote address is not allowed, IP:' . $_SERVER['REMOTE_ADDR']);
    CRM_Core_Error::debug_var('eu.caltha.payeezy warning serialized post', serialize($_POST));
  }

}
