<?php

class CRM_Payeezy_Log {

  /**
   * @param string $variable
   */
  public static function add($variable) {
    $query = "INSERT INTO civicrm_payeezy_log (variable, ip) VALUES (%1, %2)";
    $params = [
      1 => [$variable, 'String'],
      2 => [$_SERVER['REMOTE_ADDR'], 'String'],
    ];
    CRM_Core_DAO::executeQuery($query, $params);
  }

}
