<?php

class CRM_Payeezy_ResponseService extends CRM_Payeezy_Service {

  /**
   * Prepare array with required params for response service.
   *
   * @param array $processor Given processor
   * @param array $params
   *
   * @return array
   */
  public static function prepare($processor, $params) {
    $request = array();
    $request['pos_id'] = $processor['user_name'];
    $request['order_id'] = $params['invoiceID'];
    $request['session_id'] = $params['session_id'];
    $request['amount'] = self::getAmount($params['amount']);
    $request['response_code'] = $params['response_code'];
    $request['transaction_id'] = $params['transaction_id'];
    $request['cc_number_hash'] = $params['cc_number_hash'];
    $request['bin'] = $params['bin'];
    $request['card_type'] = $params['card_type'];
    $request['auth_code'] = $params['auth_code'];
    return self::orderFilter($request, CRM_Payeezy_Dict::$serviceResponseKeys);
  }
}
