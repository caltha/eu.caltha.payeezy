<?php

class CRM_Payeezy_Dict {

  const SERVICE_REQUEST = 1;
  const SERVICE_CHANNEL_REQUEST = 2;
  const SERVICE_RESPONSE = 4;
  const SERVICE_ERROR = 8;

  public static $serviceRequestKeys = array(
    'pos_id',
    'order_id',
    'session_id',
    'amount',
    'currency',
    'test',
    'language',
    'client_ip',
    'street',
    'street_n1',
    'street_n2',
    'addr2',
    'addr3',
    'city',
    'postcode',
    'country',
    'email',
    'ba_firstname',
    'ba_lastname',
    'merchant_label',
  );

  public static $serviceChannelRequestKeys = array(
    'pos_id',
    'payment_method',
    'channel_code',
    'order_id',
    'session_id',
    'amount',
    'currency',
    'test',
    'language',
    'client_ip',
    'street',
    'street_n1',
    'street_n2',
    'addr2',
    'addr3',
    'city',
    'postcode',
    'country',
    'email',
    'ba_firstname',
    'ba_lastname',
    'merchant_label',
  );

  public static $serviceResponseKeys = array(
    'pos_id',
    'order_id',
    'session_id',
    'amount',
    'response_code',
    'transaction_id',
    'cc_number_hash',
    'bin',
    'card_type',
    'auth_code',
  );

  public static $serviceErrorKeys = array(
    'pos_id',
    'order_id',
    'session_id',
    'amount',
    'message',
    'err_code',
    'transaction_id',
  );
}
