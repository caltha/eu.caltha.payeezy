<?php

class CRM_Payeezy_Model {

  const CUSTOM_GROUP_NAME_PAYEEZY_REQUIREMENTS = 'payeezy_requirements';
  const PREFIX = 'custom_';

  /**
   * @param $name
   *
   * @return string
   * @throws \CiviCRM_API3_Exception
   */
  private static function getCustomGroupPayeezyRequirements($name) {
    $result = civicrm_api3('CustomField', 'get', [
      'sequential' => 1,
      'custom_group_id' => self::CUSTOM_GROUP_NAME_PAYEEZY_REQUIREMENTS,
      'name' => $name,
    ]);

    return self::PREFIX . $result['id'];
  }

  /**
   * @return string
   * @throws \CiviCRM_API3_Exception
   */
  public static function language() {
    $key = __CLASS__ . __FUNCTION__;
    $cache = Civi::cache()->get($key);
    if (!isset($cache)) {
      $id = self::getCustomGroupPayeezyRequirements('language');
      Civi::cache()->set($key, $id);
      return $id;
    }

    return $cache;
  }

}
