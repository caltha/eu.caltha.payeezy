<?php

/**
 * Error codes from message 'service error'
 */
class CRM_Payeezy_ErrorCode {
  const SERVICE_NOT_SUPPORTED = '001',
    PAYMENT_TYPE_NOT_SUPPORTED = '004',
    CANCELATION = '005',
    SYSTEM_ERROR = '006',
    INCOMPLETE_MERCHANT_CONF = '007',
    INVALID_REQUEST = '009',
    UNKNOWN_POS_ID = '010',
    TRANSACTION_DECLINED = '011',
    TRANSACTION_DUPLICATED = '013',
    PAYMENT_NOT_EXIST = '014';

  public static $ids = array(
    self::SERVICE_NOT_SUPPORTED,
    self::PAYMENT_TYPE_NOT_SUPPORTED,
    self::CANCELATION,
    self::SYSTEM_ERROR,
    self::INCOMPLETE_MERCHANT_CONF,
    self::INVALID_REQUEST,
    self::UNKNOWN_POS_ID,
    self::TRANSACTION_DECLINED,
    self::TRANSACTION_DUPLICATED,
    self::PAYMENT_NOT_EXIST,
  );

  public static $labels = array(
    self::SERVICE_NOT_SUPPORTED => 'Ten sklep nie umożliwia realizacji płatności eCommerce',
    self::PAYMENT_TYPE_NOT_SUPPORTED => 'Ten rodzaj płatności nie jest wspierany',
    self::CANCELATION => 'Anulowanie',
    self::SYSTEM_ERROR => 'Błąd systemu',
    self::INCOMPLETE_MERCHANT_CONF => 'Niekompletna konfiguracja Akceptanta',
    self::INVALID_REQUEST => 'Zapytanie transakcyjne ma błędne dane (w tym niepoprawnie obliczona suma kontrolna)',
    self::UNKNOWN_POS_ID => 'pos_id nie jest skonfigurowane w systemie FDP',
    self::TRANSACTION_DECLINED => 'Transakcja została odrzucona',
    self::TRANSACTION_DUPLICATED => 'Transakcja została juz dokonana',
    self::PAYMENT_NOT_EXIST => 'Płatności nie istnieje w systemie',
  );
}
