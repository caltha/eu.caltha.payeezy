<?php

class CRM_Payeezy_ControlData {

  /**
   * Calculate controlData value for service request.
   *
   * @param string $sharedKey Shared key from configuration of payment processor
   * @param string $params Params requests to Payeezy web service.
   *
   * @return string
   */
  public static function calculateServiceRequestControlData($sharedKey, $params) {
    $stringParams = self::prepareServiceRequestStringParams($params);
    return self::calculateControlData($sharedKey, $stringParams);
  }


  /**
   * Calculate controlData value for service response.
   *
   * @param string $sharedKey Shared key from configuration of payment processor
   * @param string $params Params from Payeezy web service.
   *
   * @return string
   */
  public static function calculateServiceResponseControlData($sharedKey, $params) {
    $stringParams = self::prepareServiceResponseStringParams($params);
    return self::calculateControlData($sharedKey, $stringParams);
  }


  /**
   * Prepare string params for service request.
   *
   * @param array $params
   *
   * @return string
   */
  private static function prepareServiceRequestStringParams($params) {
    return self::prepareStringParams(CRM_Payeezy_Dict::$serviceRequestKeys, $params);
  }


  /**
   * Prepare string params for service request.
   *
   * @param array $params
   *
   * @return string
   */
  private static function prepareServiceResponseStringParams($params) {
    return self::prepareStringParams(CRM_Payeezy_Dict::$serviceResponseKeys, $params);
  }


  /**
   * Prepare string by given keys and params.
   *
   * @param array $keys array of keys, for example $serviceRequestKeys
   * @param array $params
   *
   * @return string
   */
  private static function prepareStringParams($keys, $params) {
    $strings = array();
    foreach ($keys as $key) {
      if (array_key_exists($key, $params) && $params[$key]) {
        $strings[] = $key . '=' . $params[$key];
      }
    }
    return implode('&', $strings);
  }


  /**
   * Calculate controlData value.
   *
   * @param string $sharedKey Shared key from configuration of payment processor
   * @param string $stringParams
   *
   * @return string
   */
  private static function calculateControlData($sharedKey, $stringParams) {
    $saltBin = self::calculateBinary($sharedKey);
    return hash("sha256", $stringParams.$saltBin);
  }


  /**
   * Calculate binary of shared key.
   *
   * @param string $sharedKey
   *
   * @return string
   */
  private static function calculateBinary($sharedKey) {
    $hexLenght = strlen($sharedKey);
    $saltBin = "";
    for ($x = 1; $x <= $hexLenght / 2; $x++) {
      $saltBin .= (pack("H*", substr($sharedKey, 2 * $x - 2, 2)));
    }
    return $saltBin;
  }
}
