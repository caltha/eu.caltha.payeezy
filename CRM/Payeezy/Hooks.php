<?php

class CRM_Payeezy_Hooks {

  static $null = NULL;

  static function alterPageRun(&$page, $pageClass) {
    return CRM_Utils_Hook::singleton()->invoke(['page', 'pageClass'], $page, $pageClass, self::$null, self::$null, self::$null, self::$null, 'civicrm_payeezyAlterPageRun');
  }
}
