<?php

/**
 * Collection of upgrade steps.
 */
class CRM_Payeezy_Upgrader extends CRM_Extension_Upgrader_Base {
  public function install() {
    $data = array(
      'payment_instrument' => array(
        'values' => array(
          'payeezy' => array(
            'label' => 'Payeezy',
            'is_default' => 0,
          ),
        ),
      ),
    );
    $options = new CRM_Payeezy_Options($data);
    $options->install();
  }

  // fixme it is doesn't called during installation
  /**
   * @return bool
   */
  public function postInstall() {
    $this->upgrade_110_custom_group();
    $this->upgrade_120_log();
    return TRUE;
  }

  /**
   * @return bool
   */
  public function upgrade_110_custom_group() {
    $this->executeCustomDataFile('xml/payeezy-requirements.xml');

    return TRUE;
  }

  public function upgrade_120_log() {
    $sqlQuery = file_get_contents(__DIR__ . '/../../sql/120-log.sql', TRUE);
    $this->executeCustomSql($sqlQuery);

    return TRUE;
  }

  /**
   * @param $sqlQuery
   */
  private function executeCustomSql($sqlQuery) {
    $string = CRM_Utils_File::stripComments($sqlQuery);
    $queries = preg_split('/@@@$/m', $string);
    foreach ($queries as $query) {
      $query = trim($query);
      if (!empty($query)) {
        CRM_Core_DAO::executeQuery($query);
      }
    }
  }

}
