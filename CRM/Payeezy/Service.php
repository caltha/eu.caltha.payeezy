<?php

class CRM_Payeezy_Service {

  /**
   * Add controlData to the end of array.
   *
   * @param $request
   * @param $controlData
   *
   * @return mixed
   */
  public static function addControlData($request, $controlData) {
    $request['controlData'] = $controlData;
    return $request;
  }

  /**
   * Order by required sequence and filter by not empty values.
   *
   * @param array $request
   * @param array $order
   *
   * @return array
   */
  protected static function orderFilter($request, $order) {
    $req = array();
    foreach ($order as $key) {
      if (array_key_exists($key, $request) && !empty($request[$key])) {
        $req[$key] = $request[$key];
      }
    }
    return $req;
  }

  /**
   * Convert amount to lower unit.
   *
   * @param $civiAmount
   *
   * @return mixed
   */
  protected static function getAmount($civiAmount) {
    return number_format($civiAmount * 100, 0, '', '');
  }

  /**
   * Convert is_test param to Y or N.
   *
   * @param $test
   *
   * @return mixed
   */
  protected static function getTest($test) {
    $map = array(
      1 => 'Y',
      0 => 'N',
    );
    return $map[$test];
  }

  /**
   * Get email by contact id.
   *
   * @param int $contactId
   *
   * @return string
   * @throws \CiviCRM_API3_Exception
   */
  protected static function getEmail($contactId) {
    $params = array(
      'sequential' => 1,
      'contact_id' => $contactId,
      'is_primary' => 1,
    );
    $result = civicrm_api3('Email', 'get', $params);
    if ($result['count'] == 1) {
      return $result['values'][0]['email'];
    }
    return '';
  }

  /**
   * The assumption is that contribution page has a campaign with language.
   *
   * @param $contributionPageId
   *
   * @return string pl | en
   */
  protected static function getLanguage($contributionPageId) {
    $qry = "SELECT IFNULL(pr.language, 'pl') language
            FROM civicrm_contribution_page cp
              LEFT JOIN civicrm_value_payeezy_requirements pr ON cp.campaign_id = pr.entity_id
            WHERE cp.id = %1";
    $params = [
      1 => [$contributionPageId, 'Integer'],
    ];
    return CRM_Core_DAO::singleValueQuery($qry, $params);
  }

}
