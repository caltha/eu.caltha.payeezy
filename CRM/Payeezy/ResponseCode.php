<?php

/**
 * Response codes used in ePrzelew.
 */
class CRM_Payeezy_ResponseCode {
  const ZAINICJOWANA = 20,
    NIEUDANA = 21,
    WERYFIKACJA_NIEUDANA = 25,
    ZAKONCZONA = 30,
    ODRZUCONA = 40,
    ZATWIERDZONA_DO_ROZLICZENIA = 35,
    ODMOWA_ROZLICZENIA = 31,
    ROZLICZENIE_ZAAKCEPTOWANE = 50,
    ROZLICZENIE_NIEUDANE = 51,
    ANULOWANA = 99;

  public static $codeIds = array(
    self::ZAINICJOWANA,
    self::WERYFIKACJA_NIEUDANA,
    self::ZAKONCZONA,
    self::ODRZUCONA,
    self::ZATWIERDZONA_DO_ROZLICZENIA,
    self::ODMOWA_ROZLICZENIA,
    self::ROZLICZENIE_ZAAKCEPTOWANE,
    self::ROZLICZENIE_NIEUDANE,
    self::ANULOWANA,
  );

  public static $codeLabels = array(
    self::ZAINICJOWANA => 'Zainicjowana',
    self::WERYFIKACJA_NIEUDANA => 'Weryfikacja nieudana',
    self::ZAKONCZONA => 'Zakończona',
    self::ODRZUCONA => 'Odrzucona',
    self::ZATWIERDZONA_DO_ROZLICZENIA => 'Zatwierdzona do rozliczenia',
    self::ODMOWA_ROZLICZENIA => 'Odmowa rozliczenia',
    self::ROZLICZENIE_ZAAKCEPTOWANE => 'Rozliczenie zaakceptowane',
    self::ROZLICZENIE_NIEUDANE => 'Rozliczenie nieudane',
    self::ANULOWANA => 'Anulowana',
  );
}
