<?php

class CRM_Payeezy_RequestService extends CRM_Payeezy_Service {

  /**
   * Prepare array with required params for request service.
   *
   * @param array $processor Given processor
   * @param array $params
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public static function prepare($processor, $params) {
    $request = array();
    $request['pos_id'] = $processor['user_name'];
    $request['order_id'] = $params['invoiceID'];
    $request['session_id'] = $params['session_id'];
    $request['amount'] = self::getAmount($params['amount']);
    $request['currency'] = 'PLN';
    $request['test'] = self::getTest($processor['is_test']);
    $request['language'] = CRM_Utils_Array::value('language', $params) ?: self::getLanguage($params['contributionPageID']);
    $request['client_ip'] = $_SERVER['REMOTE_ADDR'];
    $request['country'] = 'PL';
    $request['email'] = CRM_Utils_Array::value('email', $params) ?: self::getEmail($params['contactID']);
    return self::orderFilter($request, CRM_Payeezy_Dict::$serviceRequestKeys);
  }
}
