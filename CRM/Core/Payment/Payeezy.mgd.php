<?php
return array (
  0 =>
  array (
    'name' => 'Payeezy',
    'entity' => 'PaymentProcessorType',
    'params' =>
    array (
      'version' => 3,
      'name' => 'Payeezy',
      'title' => 'Payeezy',
      'description' => 'Payeezy Payment Processor',
      'class_name' => 'Payment_Payeezy',
      'user_name_label' => 'Identyfikator sklepu internetowego (pos_id)',
      'password_label' => 'Klucz współdzielony (shared_key)',
      'url_site_default' => 'https://vpos.polcard.com.pl/vpos/ecom/service.htm',
      'url_site_test_default' => 'https://vpos.polcard.com.pl/vpos/ecom/service.htm',
      'url_button_default' => 'https://vpos.polcard.com.pl/banners/fdp-button_payeezy_140x35px.png',
      'url_button_test_default' => 'https://vpos.polcard.com.pl/banners/fdp-button_payeezy_140x35px.png',
      'billing_mode' => 4, // notify
      'is_default' => 0,
      'is_recur' => 0,
      'payment_type' => 1,
    ),
  ),
  1 =>
    array (
      'module' => 'eu.caltha.payeezy',
      'name' => 'PayeezyFinancialType',
      'entity' => 'FinancialType',
      'params' =>
        array (
          'version' => 3,
          'name' => 'Paid by Payeezy',
          'is_deductible' => 0,
          'is_reserved' => 0,
          'is_active' => 1,
        ),
    ),
);
